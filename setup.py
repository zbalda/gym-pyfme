from setuptools import setup

setup(name='gym_pyfme',
      version='0.0.1',
      description='OpenAI Gym environment for AeroPython PyFME',
      url='https://gitlab.com/zbalda/gym-pyfme',
      install_requires=['gym', 'numpy>=1.10.4', 'sympy>=1.1.0', 'pandas', 'pynput', 'Pillow', 'tqdm']
)
