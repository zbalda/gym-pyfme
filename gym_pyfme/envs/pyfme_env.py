import numpy as np
import math
from math import sqrt, pow, sin, cos
from sympy import symbols, Matrix, solve_linear_system
from sympy.abc import x, y, z

from gym_pyfme.envs.shared.controls import Manual
from gym_pyfme.envs.shared.reward import Reward, Default
from gym_pyfme.envs.shared.path import Path
from gym_pyfme.envs.shared.render import Renderer

import gym
from gym import error, spaces, utils
from gym.utils import seeding

from pyfme.environment.atmosphere import ISA1976
from pyfme.environment.gravity import VerticalConstant
from pyfme.environment.wind import NoWind
from pyfme.environment import Environment

from pyfme.aircrafts import Cessna172
from pyfme.simulator import Simulation

from pyfme.models.state.position import EarthPosition
from pyfme.utils.trimmer import steady_state_trim
from pyfme.utils.input_generator import Constant
from pyfme.models import EulerFlatEarth

import os

# time step (seconds)
DT = 0.1

# action multiplier
ACTION_ALPHA = 0.01  # action multiplier

# cutoff values
AIRSPEED_CUTOFF = 90  # m/s -> 175 knots
ALTITUDE_CUTOFF = 25  # meters
DISPLACEMENT_CUTOFF = 500  # meters
GFORCE_CUTOFF = 4
VTHETA_DISP_CUTOFF = 1  # radians
VPSI_DISP_CUTOFF = 1  # radians


class PyfmeEnv(gym.Env):
    metadata = {'render_steps.modes': ['human']}

    def __init__(self):
        self._seed()
        self.controls = {'elevator': Manual(0),'aileron': Manual(0),'rudder': Manual(0),'throttle': Manual(0)}
        self.action_space = spaces.Box(-1, +1, (4,), dtype=np.float32)
        self.aircraft = Cessna172()
        self.environment = self._create_environment()
        self.reward = Default()
        self.path = None
        self.renderer = Renderer()
        self.simulation = None
        self.current_state = None
        self.previous_state = None
        self.partial_previous_state = None
        self.results = None

    def _seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def _create_environment(self):
        atmosphere = ISA1976()
        gravity = VerticalConstant()
        wind = NoWind()
        return Environment(atmosphere, gravity, wind)

    def create_path(self, xoft, yoft, zoft, soft):
        """
        Using `t = symbols('t')` from sympy,
        Assign x, y, z, and s relative to t
        :param xoft:
        :param yoft:
        :param zoft:
        :param soft:
        """
        equations = {
            'xoft': xoft,
            'yoft': yoft,
            'zoft': zoft,
            'soft': soft
        }
        self.path = Path(equations)

    def _create_simulation(self):
        pos = EarthPosition(x=0, y=0, height=2500)
        controls0 = {'delta_elevator': 0, 'delta_aileron': 0, 'delta_rudder': 0, 'delta_t': 0.5}
        trimmed_state, trimmed_controls = steady_state_trim(
            self.aircraft,
            self.environment,
            pos,
            0,  #psi
            52,  #tas
            controls0
        )
        self.environment.update(trimmed_state)
        self.controls['elevator'].set_value(trimmed_controls['delta_elevator'])
        self.controls['aileron'].set_value(trimmed_controls['delta_aileron'])
        self.controls['rudder'].set_value(trimmed_controls['delta_rudder'])
        self.controls['throttle'].set_value(trimmed_controls['delta_t'])
        controls = controls = {
            'delta_elevator': self.controls['elevator'],
            'delta_aileron': self.controls['aileron'],
            'delta_rudder': self.controls['rudder'],
            'delta_t': self.controls['throttle']
        }
        system = EulerFlatEarth(t0=0, full_state=trimmed_state)
        return Simulation(self.aircraft, system, self.environment, controls, DT)

    def step(self, action):
        self._take_action(action)
        self._propagate()
        self.previous_state = self.current_state
        self.current_state = self._get_state()
        reward = self._get_reward()
        done, reason = self._is_done()
        if done: print(reason)
        return self.current_state, reward, done, {}

    def reset(self):
        self.simulation = self._create_simulation()
        self.current_state = None
        self.previous_state = None
        self.partial_previous_state = None
        self.results = None

        if self.current_state:
            return self._get_state()

        return np.array([1 for x in range(35)])

    def render(self, mode='human'):
        """
        Renders the current state of the plane in the environment.
        :param mode: Not currently used
        """
        return self.renderer.render(self.current_state, self._get_position())

    def reset_render(self):
        """
        Restarts the renderer and deletes the current render.
        """
        self.renderer = Renderer()

    def export_render(self, path=os.getcwd()):
        """
        Exports the render gif.
        Path is absolute.
        Not passing a path will export to your cwd
        :param path:
        """
        print("Exporting rendered gif to ", path)
        self.renderer.close()
        self.renderer.export_gif(path + '/render.gif')

    def close(self):
        pass

    def _take_action(self, action):
        action_np = np.clip(action, -1, +1).astype(np.float32)
        assert len(action_np) == 4
        elevator = np.clip(
            self.controls['elevator'].get_value() + ACTION_ALPHA*action_np[0],
            self.aircraft.control_limits['delta_elevator'][0],
            self.aircraft.control_limits['delta_elevator'][1]
        )
        aileron = np.clip(
            self.controls['aileron'].get_value() + ACTION_ALPHA*action_np[1],
            self.aircraft.control_limits['delta_aileron'][0],
            self.aircraft.control_limits['delta_aileron'][1]
        )
        rudder = np.clip(
            self.controls['rudder'].get_value() + ACTION_ALPHA*action_np[2],
            self.aircraft.control_limits['delta_rudder'][0],
            self.aircraft.control_limits['delta_rudder'][1]
        )
        throttle = np.clip(
            self.controls['throttle'].get_value() + ACTION_ALPHA*action_np[3],
            self.aircraft.control_limits['delta_t'][0],
            self.aircraft.control_limits['delta_t'][1]
        )
        self.controls['elevator'].set_value(elevator)
        self.controls['aileron'].set_value(aileron)
        self.controls['rudder'].set_value(rudder)
        self.controls['throttle'].set_value(throttle)

    def _propagate(self):
        self.results = self.simulation.propagate(self.simulation.time+DT)

    """
    Returns
    -------
    state
        state[0]: theta - pitch angle
        state[1]: phi - roll angle
        state[2]: psi - compass direction angle
        state[3]: height - units(meters)
        state[4]: airspeed - units(m/s)
        state[5]: speed - units(m/s)
        state[6]: down_gs - downward g-forces on pilot
        state[7]: x_gs - left/right g-forces on pilot
        state[8]: y_gs - back/forward g-forces on pilot
        state[9]: x_disp - difference between current and target horizontal position
        state[10]: y_disp - difference between current and target vertical position
        state[11]: vtheta_disp - difference between current and target pitch angle
        state[12]: vpsi_disp - difference between current and target compass direction angle
        state[13]: speed_disp - difference between current and target speed
        state[14] - state[27]: dt - state 0-13 (now) - state 0-13 (previous timestep)
        state[28]: theta_dt - rate of change of altitude of target path at target point
        state[29]: psi_dt - rate of change of direction of target path at target point
        state[30]: speed_dt - rate of change of speed at target point on target path
        state[31]: elevator - range(-0.4538, 0.4887)
        state[32]: aileron - range(-0.2618, 0.3491)
        state[33]: rudder - range(-0.2793, 0.2793)
        state[34]: throttle - range(0, 1)
    """
    def _get_state(self):
        # Orientation, height, airspeed
        theta = self.results.iloc[-1]['theta']
        phi = self.results.iloc[-1]['phi']
        psi = self.results.iloc[-1]['psi']
        height = self.results.iloc[-1]['height']
        airspeed = self.results.iloc[-1]['TAS']

        # True speed
        v_down = self.results.iloc[-1]['v_down']
        v_east = self.results.iloc[-1]['v_east']
        v_north = self.results.iloc[-1]['v_north']
        speed = sqrt(pow(v_north, 2) + sqrt(pow(v_down, 2) + pow(v_east, 2)))

        # G-forces
        down_gs, x_gs, y_gs = self._get_gforces(theta, phi, psi)

        assert self.path, 'Path must be created with create_path'
        # How far it is off its path and how the path is changing
        delta, path = self.path.delta(self.simulation.system.full_state)

        # All state properties we want to compute dt for
        state = np.array([
            theta, phi, psi,
            height, airspeed, speed,
            down_gs, x_gs, y_gs
        ])
        state = np.append(state, delta)

        # Compute dt
        if self.partial_previous_state is not None:
            state_dt = np.subtract(state, self.partial_previous_state)
        else:
            state_dt = np.zeros(14)
        self.partial_previous_state = state
        state = np.append(state, state_dt)

        # Target path
        state = np.append(state, path)

        # Controls
        elevator = self.controls['elevator'].get_value()
        aileron = self.controls['aileron'].get_value()
        rudder = self.controls['rudder'].get_value()
        throttle = self.controls['throttle'].get_value()
        state = np.append(state, np.array([elevator, aileron, rudder, throttle]))

        assert len(state) == 35
        return state

    def _get_reward(self):
        return self.reward._reward(self.previous_state, self.current_state, self.results)

    def get_results(self):
        return self.results

    def _get_position(self):
        return [self.results.iloc[-1]['x_earth'], self.results.iloc[-1]['y_earth']]

    def _get_displacement(self):
        x_disp = self.current_state[9]
        y_disp = self.current_state[10]
        return sqrt(x_disp**2 + y_disp**2)

    def _get_gforces(self, theta, phi, psi):
        if self.results.shape[0] >= 2:
            vx0 = self.results.iloc[-2]['v_east']
            vy0 = self.results.iloc[-2]['v_north']
            vz0 = self.results.iloc[-2]['v_down']
            vx1 = self.results.iloc[-1]['v_east']
            vy1 = self.results.iloc[-1]['v_north']
            vz1 = self.results.iloc[-1]['v_down']
            forces = [vx1-vx0, vy1-vy0, vz0-vz1]
            gforces = np.divide(forces, DT * 9.80665)
        else:
            gforces = np.array([0, 0, 0])

        # Orthogonal orientation vectors (not yet rotated by phi)
        #   Forward (f), down (d), and right (r)
        a1, a2 = sin(math.pi/2 - theta), sin(math.pi - theta)
        a3, a4 = cos(psi), sin(psi)
        fx, fy, fz = a3*a1, a4*a1, cos(math.pi/2 - theta)
        dx, dy, dz = a3*a2, a4*a2, cos(math.pi - theta)
        rx, ry, rz = cos(psi - math.pi/2), sin(psi - math.pi/2), 0

        # Rotate orientation vectors by phi
        #   http://paulbourke.net/geometry/rotate/
        d = sqrt(fy**2 + fz**2)
        sphi, cphi = sin(phi), cos(phi)
        RX = Matrix((
            (1, 0, 0, 0),
            (0, fz/d, -fy/d, 0),
            (0, fy/d, fz/d, 0),
            (0, 0, 0, 1)
        ))
        RXI = Matrix((
            (1, 0, 0, 0),
            (0, fz/d, fy/d, 0),
            (0, -fy/d, fz/d, 0),
            (0, 0, 0, 1)
        ))
        RY = Matrix((
            (d, 0, -fx, 0),
            (0, 1, 0, 0),
            (fx, 0, d, 0),
            (0, 0, 0, 1)
        ))
        RYI = Matrix((
            (d, 0, fx, 0),
            (0, 1, 0, 0),
            (-fx, 0, d, 0),
            (0, 0, 0, 1)
        ))
        RZ = Matrix((
            (cphi, -sphi, 0, 0),
            (sphi, cphi, 0, 0),
            (0, 0, 1, 0),
            (0, 0, 0, 1)
        ))
        M = RXI*RYI*RZ*RY*RX
        D = M*Matrix((dx,dy,dz,1))
        R = M*Matrix((rx,ry,rz,1))

        # Solve system to get the forces on the aircraft's coordinate system
        system = Matrix((
            (fx, D[0], R[0], gforces[0]),
            (fy, D[1], R[1], gforces[1]),
            (fz, D[2], R[2], gforces[2] + 1)
        ))
        solution = solve_linear_system(system, x, y, z)
        return solution[y], -solution[z], -solution[x]

    def _is_done(self):
        reason = None
        if self.current_state[3] < ALTITUDE_CUTOFF:
            reason = "Episode done after altitude fell below " + str(ALITUDE_CUTOFF)
        elif self.current_state[4] > AIRSPEED_CUTOFF:
            reason = "Episode done after airspeed passed " + str(AIRSPEED_CUTOFF)
        elif self._get_displacement() > DISPLACEMENT_CUTOFF:
            reason = "Episode done after displacement ("
            reason += str(round(self.current_state[9], 2)) + ","
            reason += str(round(self.current_state[10], 2)) + ")"
            reason += " passed " + str(DISPLACEMENT_CUTOFF)
        elif abs(self.current_state[6]) > GFORCE_CUTOFF:
            reason = "Episode done after downward g-forces ("
            reason += str(self.current_state[6]) + ")"
            reason += " passed " + str(GFORCE_CUTOFF)
        elif abs(self.current_state[11]) > VTHETA_DISP_CUTOFF:
            reason = "Episode done after velocity theta displacement ("
            reason += str(self.current_state[11]) + ")"
            reason += " passed " + str(VTHETA_DISP_CUTOFF)
        elif abs(self.current_state[12]) > VPSI_DISP_CUTOFF:
            reason = "Episode done after velocity psi displacement ("
            reason += str(self.current_state[12]) + ")"
            reason += " passed " + str(VPSI_DISP_CUTOFF)
        return reason != None, reason

    def print_state(self):
        s = "=============================="
        s += "\nAircraft"
        s += "\n   Position:      ("
        s += str(round(self.results.iloc[-1]['x_earth'], 1)) + ","
        s += str(round(self.results.iloc[-1]['y_earth'], 1)) + ","
        s += str(round(self.results.iloc[-1]['height'], 1)) + ")"
        s += " (x,y,z)"
        s += "\n   Velocity:      ("
        s += str(round(self.results.iloc[-1]['v_north'], 1)) + ","
        s += str(round(self.results.iloc[-1]['v_east'], 1)) + ","
        s += str(round((-1)*self.results.iloc[-1]['v_down'], 1)) + ")"
        s += " (vx, vy, vz)"
        s += "\n   Orientation:   ("
        s += str(round(self.current_state[0], 2)) + ","
        s += str(round(self.current_state[2], 2)) + ","
        s += str(round(self.current_state[1], 2)) + ")"
        s += " (pitch, direction, roll)"
        s += "\n   G-Forces:      ("
        s += str(round(self.current_state[6], 2)) + ", "
        s += str(round(self.current_state[7], 2)) + ", "
        s += str(round(self.current_state[8], 2)) + ")"
        s += " (down, forward, right)"
        s += "\n   Speed:         ("
        s += str(round(self.current_state[4], 2)) + ","
        s += str(round(self.current_state[5], 2)) + ")"
        s += " (airspeed, speed)"
        s += "\nDisplacement"
        s += "\n   horizontal:   " + str(round(self.current_state[9], 2))
        s += "\n   vertical:     " + str(round(self.current_state[10], 2))
        s += "\n   theta:        " + str(round(self.current_state[11], 2)) + " (pitch)"
        s += "\n   psi:          " + str(round(self.current_state[12], 2)) + " (direction)"
        s += "\n   speed:        " + str(round(self.current_state[13], 2))
        s += "\nPath DT (theta, psi, speed): ("
        s += str(round(self.current_state[28], 2)) + ", "
        s += str(round(self.current_state[29], 2)) + ", "
        s += str(round(self.current_state[30], 2)) + ")"
        s += "\nControls"
        s += "\n   elevator:    " + str(round(self.current_state[31], 2))
        s += "\n   aileron:     " + str(round(self.current_state[32], 2))
        s += "\n   rudder:      " + str(round(self.current_state[33], 2))
        s += "\n   throttle:    " + str(round(self.current_state[34], 2))
        s += "\n==============================\n"
        print(s)
