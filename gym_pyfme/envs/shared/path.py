import numpy as np
import math
from math import atan
from sympy import sqrt, lambdify, symbols, solve, sin, cos
from sympy.solvers.solveset import linsolve
from sympy.plotting import plot
from sympy.plotting import plot3d_parametric_line

from pyfme.models.state import AircraftState

class Path(object):

    def __init__(self, equations):
        # Symbols
        x, y, z, t = symbols('x y z t')

        # Path parametric equations and derivatives
        self.xoft_eqn = equations['xoft']
        self.yoft_eqn = equations['yoft']
        self.zoft_eqn = equations['zoft']
        self.ddt_xoft_eqn = self.xoft_eqn.diff(t)
        self.ddt_yoft_eqn = self.yoft_eqn.diff(t)
        self.ddt_zoft_eqn = self.zoft_eqn.diff(t)
        self.xoft = lambdify(t, self.xoft_eqn)
        self.yoft = lambdify(t, self.yoft_eqn)
        self.zoft = lambdify(t, self.zoft_eqn)
        self.ddt_xoft = lambdify(t, self.ddt_xoft_eqn)
        self.ddt_yoft = lambdify(t, self.ddt_yoft_eqn)
        self.ddt_zoft = lambdify(t, self.ddt_zoft_eqn)

        # Speed parametric equations and derivatives
        self.speed_eqn = equations['soft']
        self.ddt_speed_eqn = self.speed_eqn.diff(t)
        self.speed = lambdify(t, self.speed_eqn)
        self.ddt_speed = lambdify(t, self.ddt_speed_eqn)

        # Distance (between aircraft and path) equation and derivatives
        self.distance_eqn = sqrt(
            (self.xoft_eqn - x)**2 +
            (self.yoft_eqn - y)**2 +
            (self.zoft_eqn - z)**2
        )
        self.ddt_distance_eqn = self.distance_eqn.diff(t)
        self.d2dt_distance_eqn = self.ddt_distance_eqn.diff(t)
        self.d2dt_distance = lambdify((x, y, z, t), self.d2dt_distance_eqn)

        # TODO: Handle multiple real solutions and continuous solution range
        # TODO: Can extreme_value return multiple solutions?
        self.extreme_value_eqns = solve(self.ddt_distance_eqn, t)
        self.extreme_value_eqn = self.extreme_value_eqns[0]
        self.extreme_value = lambdify((x, y, z), self.extreme_value_eqn)

        print("\npath.py WARNING - potential divide by 0 error (if any of these are 0):")
        print("   ddt_xp, h_p, vx_a, h_a, ddt_xp1, h_p1, ddt_xp, slope\n")

    def delta(self, state):
        """
        Parameters
        ----------
        state : AircraftState
            The complete state of the aircraft

        Returns
        -------
        delta:
            delta[0]: x_disp - Horizontal displacement between aircraft and target position
            delta[1]: y_disp - Vertical displacement between aircraft and target position
            delta[2]: theta_disp - Displacement between aircraft and target pitch angle
            delta[3]: psi_disp - Displacement between aircraft and target compass direction angle
            delta[4]: speed_disp - Displacement between aircraft and target speed
        path:
            path[0]: theta_dt - Rate of change in target pitch angle on target path
            path[1]: psi_dt - Rate of change in target compass direction on target path
            path[2]: speed_dt - Rate of change in speed on target path
        """

        # Symbols
        x, y, z = symbols('x y z')

        # Current position and velocity of aircraft (a)
        x_a = state.position.x_earth
        y_a = state.position.y_earth
        z_a = state.position.height
        vx_a = state.velocity.u
        vy_a = state.velocity.v
        vz_a = state.velocity.w

        # Extreme value (shortest distance candidate)
        # TODO: check d2dt_distance is positive (-> distance minima)
        t_ev = self.extreme_value(x_a, y_a, z_a)

        # Path (p) vectors
        xp = self.xoft(t_ev)
        yp = self.yoft(t_ev)
        zp = self.zoft(t_ev)
        ddt_xp = self.ddt_xoft(t_ev)
        ddt_yp = self.ddt_yoft(t_ev)
        ddt_zp = self.ddt_zoft(t_ev)

        # Path (p) angles
        psi_p = atan(ddt_yp/ddt_xp)
        h_p = sqrt(ddt_xp**2 + ddt_yp**2)
        theta_p = atan(ddt_zp/h_p)

        # Aircraft (a) angles
        psi_a = atan(vy_a/vx_a)
        h_a = sqrt(vx_a**2 + vy_a**2)
        theta_a = atan(vz_a/h_a)

        # Path (p) angles at time t + 1
        ddt_xp1 = self.ddt_xoft(t_ev + 1)
        ddt_yp1 = self.ddt_yoft(t_ev + 1)
        ddt_zp1 = self.ddt_zoft(t_ev + 1)
        psi_p1 = atan(ddt_yp1/ddt_xp1)
        h_p1 = sqrt(ddt_xp1**2 + ddt_yp1**2)
        theta_p1 = atan(ddt_zp1/h_p1)

        # Speed of path (p) and aircraft (a)
        speed_p = self.speed(t_ev)
        speed_a = sqrt(vx_a**2 + vy_a**2 + vz_a**2)

        # Position displacement
        # (right triangle with points PQA and right angle at Q)
        slope = ddt_yp / ddt_xp
        line_PQ = slope * (x - xp) - y + yp
        line_AQ = -1/slope * (x - x_a) - y + y_a
        Q, = linsolve([line_PQ, line_AQ], (x, y))
        PQ_distance = sqrt((xp - Q[0])**2 + (yp - Q[1])**2)
        AQ_distance = sqrt((x_a - Q[0])**2 + (y_a - Q[1])**2)
        x_disp_direction = 1
        if (slope*(x_a - xp) - y_a + yp) > 0:
            x_disp_direction = -1
        delta_z = z_a - zp
        y_disp_direction = 1 if delta_z >= 0 else -1
        x_disp = AQ_distance * x_disp_direction
        y_disp = sqrt(PQ_distance**2 + delta_z**2) * y_disp_direction

        # Angle and speed displacement
        theta_disp = theta_a - theta_p
        psi_disp = psi_a - psi_p
        speed_disp = speed_a - speed_p

        # Path change
        theta_dt = theta_p1 - theta_p
        psi_dt = psi_p1 - psi_p
        speed_dt = self.ddt_speed(t_ev)

        delta = [x_disp, y_disp, theta_disp, psi_disp, speed_disp]
        path = [theta_dt, psi_dt, speed_dt]

        return delta, path
