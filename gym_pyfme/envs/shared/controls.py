from pyfme.utils.input_generator import Control
from pyfme.utils.input_generator import vectorize_float

class Manual(Control):

    def __init__(self, offset=0):
        self.value = offset

    @vectorize_float
    def _fun(self, t):
        return self.value

    def get_value(self):
        return self.value

    def set_value(self, value):
        self.value = value

    def adjust(self, value):
        self.value += value
