from abc import abstractmethod
from math import sqrt

class Reward(object):

    @abstractmethod
    def _reward(self, prev_state, state, results):
        raise NotImplementedError

class Default(Reward):

    def __init__(self):
        self.MAX_DISPLACEMENT_REWARD = 100
        self.MAX_SPEED_REWARD = 40
        self.MAX_DOWN_GFORCE_REWARD = 20
        self.MAX_X_GFORCE_REWARD = 20
        self.MAX_Y_GFORCE_REWARD = 20
        self.DISPLACEMENT_ALPHA = 1
        self.SPEED_ALPHA = 5
        self.DOWN_GFORCE_ALPHA = 50
        self.X_GFORCE_ALPHA = 50
        self.Y_GFORCE_ALPHA = 50

    def _reward(self, prev_state, state, results):
        displacement_value = sqrt(state[9]**2 + state[10]**2) * self.DISPLACEMENT_ALPHA
        speed_value = abs(state[13]) * self.SPEED_ALPHA
        down_gforce_value = abs(state[6]-1) * self.DOWN_GFORCE_ALPHA
        x_gforce_value = abs(state[7]) * self.X_GFORCE_ALPHA
        y_gforce_value = abs(state[8]) * self.Y_GFORCE_ALPHA

        if displacement_value > 100: displacement_value = 100
        if speed_value > 100: speed_value = 100
        if down_gforce_value > 100: down_gforce_value = 100
        if x_gforce_value > 100: x_gforce_value = 100
        if y_gforce_value > 100: y_gforce_value = 100

        displacement_ratio = (100-displacement_value)/100
        speed_ratio = (100-speed_value)/100
        down_gforce_ratio = (100-down_gforce_value)/100
        x_gforce_ratio = (100-x_gforce_value)/100
        y_gforce_ratio = (100-y_gforce_value)/100

        reward = displacement_ratio * self.MAX_DISPLACEMENT_REWARD
        reward += speed_ratio * self.MAX_SPEED_REWARD
        reward += down_gforce_ratio * self.MAX_DOWN_GFORCE_REWARD
        reward += x_gforce_ratio * self.MAX_X_GFORCE_REWARD
        reward += y_gforce_ratio * self.MAX_Y_GFORCE_REWARD

        return reward
