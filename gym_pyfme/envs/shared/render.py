import os

from gym.envs.classic_control import rendering
import pandas as pd
import math
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from PIL import Image
import numpy as np
from mpl_toolkits.mplot3d.art3d import Poly3DCollection


class Renderer:
    def __init__(self):
        # adjustable params
        self.VIEWPORT_W = 1000
        self.VIEWPORT_H = 1000
        self.SCALE = 1
        self.SCALE_Z = 100

        try:
            os.system('rm {}/temp/render_steps/*.png'.format(__file__[:-10]))  # Delete old steps
        except:
            pass

    def render(self, state, pos):
        """
        Produces a frame, representing the status of the plane in state. Every call to this function produces a frame
        that is compiled into a gif, exported with env.export_render(path?)
        :param state:
        :param pos:
        :return:
        """
        # State is a dataframe with all of the information about the current state of the plane

        # Setup plot
        plt.clf()  # clear existing plot
        fig = plt.figure(1, figsize=(20, 20))

        # --- Build 3D ---
        whole = fig.add_subplot(321, projection='3d')
        whole.set_xlim([-50, 1000])
        whole.set_ylim([-1000, 1000])
        whole.set_zlim([0, 5000])

        whole.set_xlabel('X')
        whole.set_ylabel('Y')
        whole.set_zlabel('Alt')

        whole.title.set_text("Roll: {0:.0f}    Pitch: {1:.0f}    Height: {2:.0f}".format(math.degrees(state[1]), math.degrees(state[0]), float(state[3])))
        whole.scatter(pos[0], state[1], float(state[3]))

        # Add function track
        # y = np.linspace(0, 1000, 1000)
        # x = np.multiply(np.sin(np.multiply(y, 0.01)), 100)
        # z = np.subtract(1000, y)
        # whole.plot(x, y, z, 'g', label='track', alpha=0.5)

        # --- Top Down Flight Path ---
        top = fig.add_subplot(333)
        top.set_xlim([-50, 1000])
        top.set_ylim([500, -500])

        top.set_xlabel('X')
        top.set_ylabel('Y')

        scale_ratio = 100

        tri_coord = self._get_triangle([pos[0], pos[1]], state[3] / scale_ratio, 90+((-1 * math.degrees(state[2])) % 360))
        t1 = plt.Polygon(tri_coord, color='b')
        top.add_patch(t1)

        # Add function track
        # x = np.linspace(0, 1000, 1000)
        # top.plot(np.multiply(np.sin(np.multiply(x, 0.01)), 100), x, 'g', label='track', alpha=0.5)

        top.title.set_text('Heading: {0:.0f}   TAS: {1:.0f}'.format(math.degrees(state[2]), state[5]))

        # --- Rear plane view ---
        rear = fig.add_subplot(334)
        rear.set_xlim([-100, 100])
        rear.set_ylim([-100, 100])
        rear.axis('off')  # remove scale

        a_circle = plt.Circle((0, 0), 20, color='b')
        rear.add_artist(a_circle)  # Add body

        roll = math.radians((-1 * math.degrees(state[1])) % 360)  # Convert clockwise to counterclockwise and then into radians

        wing1 = self._rotate([0, 0], [-50, 0], roll)
        wing2 = self._rotate([0, 0], [50, 0], roll)
        rear.plot([wing1[0], wing2[0]], [wing1[1], wing2[1]], color='b', linewidth=5)  # Add wings

        # --- Side Plane View ---
        side_p = fig.add_subplot(335)
        side_p.set_xlim([-100, 100])
        side_p.set_ylim([-100, 100])
        side_p.axis('off')  # remove scale

        plane = self._get_triangle([-50, 0], 25, 90-math.degrees(state[0]))
        t1 = plt.Polygon(plane, color='b')
        side_p.add_patch(t1)

        # --- Side flight path view ---
        side = fig.add_subplot(336)
        side.set_xlim([0, 1000])
        side.set_ylim([0, 5000])

        side.set_xlabel('X')
        side.set_ylabel('Alt (ft)')

        side.scatter(pos[0], float(state[3]))

        # Add function track
        # x = np.linspace(0, 1000, 1000)
        # side.plot(np.subtract(1000, x), x, 'g', label='track', alpha=0.5)

        # --- Controls ---
        cont1 = fig.add_subplot(337)
        cont1.set_xlim([-0.2618, 0.3491])
        cont1.set_ylim([-0.4538, 0.4887])

        cont1.set_xlabel('Ailerons')
        cont1.set_ylabel('Elevator')

        cont1.plot([0, state[32]], [0, 0], color='g', linewidth=8)  # Plot ailerons
        cont1.plot([0, 0], [0, state[31]], color='purple', linewidth=8)  # Plot elevator

        cont2 = fig.add_subplot(338)
        cont2.set_xlim([-0.2793, 0.2793])
        cont2.set_ylim([-1, 1])

        cont2.set_xlabel('Rudder')
        cont2.set_ylabel('Throttle')

        cont2.plot([0, state[33]], [0, 0], color='blue', linewidth=8)  # Plot Rudder
        cont2.plot([0, 0], [0, state[34]], color='red', linewidth=8)  # Plot Throttle

        # --- Gs ---
        gs = fig.add_subplot(339)
        gs.set_xlim([-4, 4])
        gs.set_ylim([-4, 4])

        gs.set_xlabel('Horz Gs')
        gs.set_ylabel('Vert Gs')

        gs.title.set_text('G-forces')

        a_circle = plt.Circle((0, 0), .5, edgecolor='black', fill=False)
        gs.add_artist(a_circle)

        a_circle = plt.Circle((0, 0), 4, edgecolor='black', fill=False)
        gs.add_artist(a_circle)

        gs.scatter(state[7], state[8])

        # --- Export plot ---
        index = len(os.listdir(__file__[:-10]+'/temp/render_steps/'))
        plt.savefig(__file__[:-10]+'/temp/render_steps/{}.png'.format(index), dpi=150)

        return plt

    def _get_triangle(self, center, scale, heading):
        """
        :param center:
        :param scale:
        :param heading:
        :return: Array of points representing triangle
        """
        tri = [[0, 0], [0, 0], [0, 0]]

        # Create triangle
        # Get point
        tri[0][0] = center[0]  # x stays constant
        tri[0][1] = center[1] + 1  # y

        # Get bottom left
        tri[1][0] = center[0] - 0.5  # x
        tri[1][1] = center[1] - (math.sqrt(3)/2)  # y

        # Get bottom right
        tri[2][0] = center[0] + 0.5  # x
        tri[2][1] = center[1] - (math.sqrt(3) / 2)  # y

        # Scale triangle
        for index, point in enumerate(tri):
            tri[index] = self._scale(center, point, scale)

        # Rotate triangle
        for index, point in enumerate(tri):
            cc_head = (-1 * heading) % 360  # Convert clockwise to counterclockwise
            tri[index] = self._rotate(center, point, math.radians(cc_head))

        return tri

    def _get_triangular_prism(self, center, heading, roll):
        """
        :param center:
        :param scale:
        :param heading:
        :return: Array of points representing triangle
        """
        tri = [[0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0]]

        # Create triangle
        # Get point top
        tri[0][0] = center[0]  # x stays constant
        tri[0][1] = center[1] + 1  # y
        tri[0][2] = center[2] + 1  # z

        # Point bottom
        tri[0][0] = center[0]  # x stays constant
        tri[0][1] = center[1] + 1  # y
        tri[0][2] = center[2] - 1  # z

        # Get bottom left top
        tri[1][0] = center[0] - 0.5  # x
        tri[1][1] = center[1] - (math.sqrt(3)/2)  # y
        tri[1][2] = center[2] + 1  # z

        # Get bottom left bottom
        tri[1][0] = center[0] - 0.5  # x
        tri[1][1] = center[1] - (math.sqrt(3) / 2)  # y
        tri[1][2] = center[2] - 1  # z

        # Get bottom right top
        tri[2][0] = center[0] + 0.5  # x
        tri[2][1] = center[1] - (math.sqrt(3) / 2)  # y
        tri[2][2] = center[2] + 1  # z

        # Get bottom right bottom
        tri[2][0] = center[0] + 0.5  # x
        tri[2][1] = center[1] - (math.sqrt(3) / 2)  # y
        tri[2][2] = center[2] - 1  # z

        # Rotate triangle
        for index, point in enumerate(tri):
            cc_head = (-1 * heading) % 360  # Convert clockwise to counterclockwise
            tri[index] = self._rotate(center, point, math.radians(cc_head))

        # Roll triangle
        # TODO: Finish implementation of triangular prism

        return tri

    def _rotate(self, origin, point, angle):
        """
        Rotate a point counterclockwise by a given angle around a given origin.

        The angle should be given in radians.
        """
        ox, oy = origin
        px, py = point

        qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
        qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)
        return qx, qy

    def _scale(self, origin, point, scale):
        """
        Scale a point from an origin to a scale
        :param origin:
        :param point:
        :param scale:
        :return:
        """
        tr_pointx, tr_pointy = point[0] - origin[0], point[1] - origin[1]
        sc_pointx, sc_pointy = tr_pointx * scale, tr_pointy * scale
        return [sc_pointx + origin[0], sc_pointy + origin[1]]

    def close(self):
        """
        Generates the gif with the steps accrued by render().
        No longer clears the steps, allowing for gifs to continue after generation.
        :return:
        """
        plt.clf()  # Clear plt
        # Grab all steps and turn it into a gif
        # Create the frames
        frames = []
        imgs = ["{}/temp/render_steps/{}.png".format(__file__[:-10], index) for index in range(len(os.listdir(__file__[:-10]+'/temp/render_steps/')))]
        for im in imgs[1:]:
            new_frame = Image.open(im)
            frames.append(new_frame)

        # Save into a GIF file that loops forever
        frames[0].save(__file__[:-10]+'/temp/render.gif', format='GIF',
                       append_images=frames[1:],
                       save_all=True,
                       duration=10, loop=0)

        plt.close()

    def export_gif(self, route):
        """
        Moves the gif from a temp folder to the destination route
        :param route:
        :return:
        """
        os.system("mv {}/temp/render.gif {}".format(__file__[:-10], route))


if __name__ == '__main__':
    ''' Test code '''
    loop_len = 25

    #  Create fake data
    state = [[0 for a in range(36)] for x in range(loop_len)]
    pos = [[0 for x in range(loop_len)], [0 for y in range(loop_len)]]

    # Generate data
    for i in range(loop_len):
        state[i][3] = 1000 - (i + (np.random.random_sample() - 0.5))  # Height
        pos[1][i] = i  # y
        pos[0][i] = math.sin(i * 0.01) * 100  # x
        state[i][2] = math.cos(i * 0.01) * 45  # psi
        # ids.append(i)  # ids
        state[i][32] = math.sin(0.1 * i)  # elevator
        state[i][33] = math.cos(0.2 * i)  # ailerons
        state[i][34] = math.sin(0.05 * i)  # rudder
        state[i][35] = 0.25 * math.cos(0.025 * i) + 0.25  # throttle
        state[i][7] = math.sin(0.1 * i)  # horizontal gs
        state[i][8] = math.sin(0.2 * i)  # vertical gs
        state[i][1] = math.sin(i * 0.01) * 30  # roll
        state[i][0] = math.sin(i * 0.01) * 10  # pitch

    renderer = Renderer()
    for i, step in enumerate(state):
        print(i)
        renderer.render(step, [pos[0][i], pos[1][i]])

    renderer.close()
