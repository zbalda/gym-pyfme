from gym.envs.registration import register

register(
    id='PyFME-v0',
    entry_point='gym_pyfme.envs:PyfmeEnv',
)
