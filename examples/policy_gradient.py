from keras.layers import Dense, Activation, Input
from keras.models import Model, load_model
from keras.optimizers import Adam
import keras.backend as K
import gym
import gym_pyfme
import os
import numpy as np
from sympy import symbols


class PolicyGradient(object):
    name = None

    def __init__(self, ALPHA=0.01, GAMMA=0.98, n_actions=4, layer1_size=64, layer2_size=128, input_dims=35, fname='pg.h5'):
        self.lr = ALPHA
        self.gamma = GAMMA
        self.n_actions = n_actions
        self.fc1_dims = layer1_size
        self.fc2_dims = layer2_size
        self.input_dims = input_dims
        self.G = 0

        self.name = self.name()

        # cleared out each episode
        self.state_memory = []
        self.action_memory = []
        self.reward_memory = []

        self.policy, self.predict = self.__build_policy_network()
        self.action_space = [i for i in range(n_actions)]
        self.model_file = fname

    def __build_policy_network(self):
        input = Input(shape=(self.input_dims,))
        advantages = Input(shape=[1])
        dense1 = Dense(self.fc1_dims, activation='relu')(input)
        dense2 = Dense(self.fc2_dims, activation='relu')(dense1)
        dense3 = Dense(64, activation='relu')(dense2)
        dense4 = Dense(32, activation='relu')(dense3)
        probs = Dense(self.n_actions, activation='softmax')(dense4)

        def custom_loss(y_true, y_pred):
            out = K.clip(y_pred, 1e-8, 1-1e-8)
            log_lik = y_true*K.log(out)
            return K.sum(-log_lik*advantages)

        policy = Model(input=[input, advantages], output=[probs])
        policy.compile(optimizer=Adam(lr=self.lr), loss=custom_loss)

        predict = Model(input=[input], output=[probs])

        return policy, predict

    def act(self, observation):
        state = observation[np.newaxis, :]
        probabilities = self.predict.predict(state)[0]
        action_change = np.random.choice(self.action_space, p=probabilities)

        # Convert 8-length single action into 4-length action array
        new_action = [0, 0, 0, 0]
        if action_change == 0:
            new_action[0] = 1
        elif action_change == 1:
            new_action[1] = 1
        elif action_change == 2:
            new_action[2] = 1
        elif action_change == 3:
            new_action[3] = 10
        elif action_change == 4:
            new_action[0] = -1
        elif action_change == 5:
            new_action[1] = -1
        elif action_change == 6:
            new_action[2] = -1
        elif action_change == 7:
            new_action[3] = -10

        return new_action

    def remember(self, observation, action, reward):
        self.state_memory.append(observation)
        self.action_memory.append(action)
        self.reward_memory.append(reward)

    def fit(self):
        state_memory = np.array(self.state_memory)
        action_memory = np.array(self.action_memory)
        reward_memory = np.array(self.reward_memory)

        actions = action_memory

        G = np.zeros_like(reward_memory)
        for t in range(len(reward_memory)):
            G_sum = 0
            discount = 1
            for k in range(t, len(reward_memory)):
                G_sum += reward_memory[k]*discount
                discount *= self.gamma
            G[t] = G_sum
        mean = np.mean(G)
        G = np.array(G, dtype=np.float64)  # Convert to np float instead of sympy float
        std = 1
        if np.std(G) > 0:
            std = np.std(G)
        self.G = (G-mean)/std

        cost = self.policy.train_on_batch([state_memory, self.G], actions)

        self.state_memory = []
        self.action_memory = []
        self.reward_memory = []

    def save_model(self):
        self.policy.save(self.model_file)

    def load_model(self):
        self.policy.load_model(self.model_file)

    @staticmethod
    def name():
        return 'Policy Gradient Continuous'


class LevelClimb:
    path = None
    name = None

    def __init__(self):
        self.path = self.path()
        self.name = self.name()

    @staticmethod
    def path():
        t = symbols('t')
        xoft = t
        yoft = 0.0000000001*t
        zoft = 1.0000000001*t + 2500
        soft = 52 + 0.00001*t
        return xoft, yoft, zoft, soft

    @staticmethod
    def name():
        return 'Level Climb'

disp = []
env = gym.make('PyFME-v0')
model = PolicyGradient()
path = LevelClimb()

print("Training {} on {}".format(model.name, path.name))

# Set starting controls
action = [0, 0, 0, 0]

# Wipe previous render folder
os.system('rm -rf renders/*')

# Create path
env.create_path(*path.path)  # Choose path
gamma = 0.9
epsilon = .95

trials = 1000
trial_len = 500

render_every = 20

trial_rewards = []
for trial in range(trials):
    print("Trial:", trial)
    cur_state = env.reset()
    reward_history = []
    for step in range(trial_len):
        print("Step:", step)

        action = model.act(cur_state)  # Predict the next best action
        observation, reward, done, info = env.step(action)  # See what happens with that action

        reward_history.append(reward)
        print("Action:", action)

        # reward = reward if not done else -20
        new_state = observation

        model.remember(cur_state, action, reward)  # Remember what happened this iteration

        cur_state = new_state

        # env.print_state()
        if trial % render_every == 0:
            env.render()
        if done:
            print('*************\n------ Done was triggered ------\n*************')
            print('Trial', trial, "got average reward", np.mean(reward_history[-100:]))
            trial_rewards.append(np.mean(reward_history[-100:]))
            break

    if trial % render_every == 0:
        env.export_render()
        try:
            os.system('mkdir renders')  # Create directory
        except:
            pass  # if it doesn't exist
        os.system('mv render.gif renders/{}.gif'.format(trial))
        env.reset_render()

    model.fit()  # Fit at the end of the episode

print("Trial rewards:", trial_rewards)
results = env.get_results()

print("Closing env")
env.close()


