"""
This will take in a PyFME output and render it in xplane
"""

from xplane import xpc
import time


class Renderer:
    def __init__(self, data):
        self.data = data

    def play(self):
        with xpc.XPlaneConnect() as client:
            # Verify connection
            try:
                # If X-Plane does not respond to the request, a timeout error
                # will be raised.
                client.getDREF("sim/test/test_float")
            except:
                print("Error establishing connection to X-Plane.")
                print("Exiting...")
                return

        print(self.data)
        print(list(self.data))
        for i, step in self.data.iterrows():
            #       Lat              Lon              Alt   Pitch Roll Yaw Gear
            posi = [step['x_earth'], step['y_earth'], step['z_earth'], 0, 0, 0, 1]
            client.sendPOSI(posi)

            # Set controls
            ctrl = [step['aileron'], step['elevator'], step['rudder'], step['thrust']]  # aileron, elevator, rudder and throttle positions
            client.sendCTRL(ctrl)

            time.sleep(0.01)

