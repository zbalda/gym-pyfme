 # gym-pyfme

An OpenAI Gym Environment for [AeroPython PyFME](https://github.com/AeroPython/PyFME)

![Renderer Example](media/render.gif)

## Installation

Install the [OpenAI  Gym](https://github.com/openai/gym)


```
pip install git+https://github.com/AeroPython/PyFME.git
```

```bash
cd gym-pyfme
pip install -e .
```

## Goal

The goal of this environment is to control an aircraft (*orange*) in the [PyFME](https://github.com/AeroPython/PyFME) flight mechanics engine and follow a path (*blue*) described by parametric equations.

| | |
|---|---|
| ![Parametric Graph](media/parametric-graph-still.gif) | ![Parametric Graph](media/parametric-graph.gif)|

## Examples

| File in `gym-pyfme/examples/` | Description |
|---|---|
| [`keyboard_controls.py`](examples/keyboard_controls.py) | Uses keyboard input to control the aircraft |
| [`xplane_renderer.py`](examples/xplane_renderer.py) | Uses PyFME results and runs them in X-Plane for rendering |
| [`policy_gradient.py`](examples/policy_gradient.py) | Trains a policy gradient model using Keras and the gym. Renders every 20 trials. |

#### Basic Example

Import and create the gym environment.
```
import gym
import gym_pyfme

env = gym.make('PyFME-v0')
```

Define the `sympy` parametric equations `(x(t), y(t), z(t), speed(t))` of the path for the aircraft to follow. Each equation must have a `t` so that derivatives can be computed (sympy doesn't like constants). The initial state of the aircraft is heading level in the positive x direction at 2500 meters altitude, traveling 52 m/s. The parametric equations at t=0 should have the same or similar position/direction/speed.
```
from sympy import symbols

t = symbols('t')
env.create_path(
    t,
    0.0000000001*t,
    0.0000000001*t + 2500,
    52 + 0.00001*t
)
```

Initialize the environment with `env.reset()`, which must be called before `env.step()` can be called. Use `env.print_state()` to display the current state in the console. Use `env.render()` to generate an image for the current state. The `env.step()` method accepts an array of four numbers corresponding to elevator, aileron, rudder, and throttle adjustments respectively.

```
init_state = env.reset()
done = False
while not done:
    action = env.action_space.sample()
    observation, reward, done, info = env.step(action)
    env.print_state()
    env.render()
```
See more on: [Controls/Actions](#controls-actions), [State/Observation](#state-observation), [Reward](#reward), [Done](#done), [Renderer](#renderer)

Get the table of results with `env.get_results()`. Use `env.export_render(path)` to compile and save a `gif` of all the images created with `env.render()`. If no path is given then the gif will be saved to the current directory. Call `env.reset_render()` to clear all images and start a new gif.

```
results = env.get_results()
env.export_render(optional_relative_path)
env.close()
```

# Documentation

## Controls / Actions

| Control | Range | Action | Description |
|---|---|---|---|
| Elevator | (-0.4538, 0.4887) | action[0] | Controls the pitch of the aircraft |
| Aileron | (-0.2618, 0.3491) | action[1] | Controls the roll of the aircraft |
| Rudder | (-0.2793, 0.2793) | action[2] | Controls the yaw of the aircraft |
| Throttle | (0, 1) | action[3] | Controls the engine power |

In `env.step(action)` (pseudocode):
```
action = np.clip(action, -1, 1)
controls += action * ACTION_ALPHA
controls = np.clip(controls, ranges.lower, ranges.upper)
```

## State / Observation

| | Variable | Description |
|---|---|---|
| 0 | theta | Pitch angle in radians |
| 1 | phi | Roll angle in radians |
| 2 | psi | Compass direction angle in radians |
| 3 | height | Altitude in meters |
| 4 | airspeed | In meters per second |
| 5 | speed | In meters per second |
| 6 | down_gs | Downward g-forces on pilot |
| 7 | x_gs | G-forces a pilot would experience to their right |
| 8 | y_gs | G-forces a pilot would experience back |
| 9 | x_disp | Difference between current and target horizontal position |
| 10 | y_disp | Difference between current and target vertical position |
| 11 | vtheta_disp | Difference between current and target pitch angle |
| 12 | vpsi_disp | Difference between current and target compass direction angle |
| 13 | speed_disp | Difference between current and target speed |
| 14 - 27 | dt | state 0-13 (now) - state 0-13 (previous time step)  |
| 28 | theta_dt | Rate of change of altitude of target path at target point |
| 29 | psi_dt | Rate of change of direction of target path at target point |
| 30 | speed_dt | Rate of change of speed at target point on target path |
| 31 | elevator | Current elevator control position (between -0.4538 and 0.4887) |
| 32 | aileron | Current aileron control position (between -0.2618 and 0.3491) |
| 33 | rudder | Current rudder control position (between -0.2793 and 0.2793) |
| 34 | throttle | Current throttle control position (between 0 and 1) |

The state is designed to be independent of direction. This means that the position and velocity of the aircraft and path are not given. This is to spare the agent from having to learn 3D Euclidean space in regards to position, velocity, etc.. Rather, there is some path the aircraft should follow, some position it is at, and we only show it the difference/displacement between its current values and target values.

##### Determining a Target Point on the Path

We calculate the target point (the point where the aircraft should be) as the point on the path that is closest to the aircraft.

```
distance(x,y,z,t) = sqrt((x(t) - x)^2 + (y(t) - y)^2 + (z(t) - z)^2)
```

This equation determines the distance between any point (x,y,z) and the point on the path at t.

By taking the first derivative of distance `distance'(x,y,z,t)` we get an equation for calculating extreme values. Given some point (x,y,z) we solve `distance'(x,y,z,t) = 0` for t which gives us the extreme value(s) of t. Using the second derivative `distance''(x,y,z,t)` we can determine which of these extreme value(s) is a minimum.

This minimum gives us the point on the path that is closest to the aircraft.

##### Determining Position Displacement from Target Point

Since the state is based on displacement, we must represent **how** the aircraft is off from its target point.

![Displacement Graph](media/displacement-graph.png)

The plane shown is formed from a point on the path `(x(t), y(t), z(t))` (black dot) and the normal vector at that point `(x'(t), y'(t), z'(t))` (black vector). This plane contains the point `(x,y,z)` of the aircraft which is shown as the orange dot. The plane is oriented such that its x-axis lies on a plane parallel to the xy-plane of the simulation.

Thus, `x_disp` and `y_disp` are not the x and y displacement in the coordinate system of the simulation, but rather the x and y displacement on this plane. Computing this gives a sense of whether the aircraft is too far left or right and too far up or down.

##### Determining Orientation Displacement from Target Point

To compute orientation displacement we use polar coordinates to represent the velocity vector of the aircraft and the direction vector of the path at the target point. Traditionally, (theta,psi)=(0,0) would have a unit vector of (0,0,1). However, the way we represent theta, (theta,psi)=(0,0) yields (1,0,0). Theta changes in a direction s.t. (theta,psi)=(pi/2,0) yields (1/sqrt(2),1/sqrt(2)).
```
vtheta_disp = theta_aircraft - theta_path
vpsi_disp = psi_aircraft - psi_path
```
Computing this gives a sense of how the heading of the aircraft different than the direction of the track at the target point. It's important to note, however, that an aircraft's heading and track are often different due to wind correction. Orientation displacement is not necessarily adverse behavior.

##### Computing G-Forces

G-Forces are computed for the left/right `x_gs`, forward/back `y_gs`, and up/down `down_gs` directions of the pilots orientation. If the plane was level and slowed down quickly, the pilot would experience an increase in y_gs. If the plane was level and moved to its left quickly, the pilot would experience an increase in x_gs.

To compute g-forces from the pilots orientation we start by computing the g-forces acting on the aircraft. This is simply the accelerations in the x, y, and z direction from motion and the normal force from gravity (z+=1). Next we create three orthogonal unit vectors (forward, down, right) for the orientation of the plane. The forward vector points where the plane is facing. The down vector points down from the bottom of the plane. The right vector points out in the direction of the right wing. We then solve this system of equations to find the magnitudes of the unit orientation vectors whose sum gives us the vector of g-forces acting on the aircraft.

See [Paul Bourke](http://paulbourke.net/geometry/rotate/) for an explanation of vector rotations that are used to compute the unit orientation vectors of the aircraft.

These g-forces can be used as part of the reward function so that the agent learns to fly comfortably.

## Reward

#### Default Reward Function

| Contributor | Description |
|---|---|
| Position Displacement | How far the aircraft is from the point on the target path that is closest to the aircraft |
| Speed Displacement | Magnitude of the difference between current and target speed |
| Down G-Force Displacement | Magnitude of the difference between current downward g-forces and one g |
| Forward G-Force Displacement | Magnitude of the g-forces in the forward direction of the aircraft's orientation |
| Side G-Force Displacement | Magnitude of the g-forces in the sideways direction of the aircraft's orientation |

The target point is the point on the target path that is closest to the aircraft. The closer the aircraft is to the target point, the higher the reward. The closer it is to the target speed at the target point, the higher the reward. The closer the aircraft is to one downward G, the higher reward. The closer the aircraft is to zero forward and zero side g-forces, the higher the reward.

This reward function is based on how well the aircraft is doing at its current point in time. It does not base the reward on how much it improves itself from the last state.

#### Custom Reward Function

In `gym_pyfme/envs/shared/reward.py`:

```
class Reward(object):

    @abstractmethod
    def _reward(self, prev_state, state, results):
        raise NotImplementedError
```

To create a new reward function, add a class in `reward.py` that inherits from `Reward` and implement `_reward()`:
```
class CustomReward(Reward):

    def __init__(self):
        # Initialize
        pass

    def _reward(self, prev_state, state, results):
        # Implement
        return 0
```
To use a different reward function, simply modify the reward in `gym_pyfme/envs/pyfme_env.py`:
```
class PyfmeEnv(gym.Env):
    ...
    def __init__(self):
        ...
        self.reward = Default() # Change Default() to CustomReward()
        ...
```

## Done

`env.step()` returns `done = True` when any of these constants declared in `gym_pyfme/envs/pyfme_env.py` are reached:

| Constant | Default Value | Cutoff If |
|---|---|---|
| AIRSPEED_CUTOFF | 90 m/s | Airspeed passes this value |
| ALTITUDE_CUTOFF | 25 meters | Altitude falls below this value |
| DISPLACEMENT_CUTOFF | 500 meters | Distance between aircraft and target point on target path passes this value |
| GFORCE_CUTOFF | 4 g-forces | Magnitude of downward g-forces passes this value |
| VTHETA_DISP_CUTOFF | 1 radian | Magnitude of the difference between the current and target pitch angle passes this value |
| VPSI_DISP_CUTOFF | 1 radian | Magnitude of the difference between the current and target direction angle passes this value |

## Renderer

The renderer is accessed through the environment.  
- `env.render()` Generates a single frame based on the current state of the plane in the environment.
  - Call this function once every step for best results.
- `env.export_render(optional_relative_path)` Exports the gif to the supplied relative path of a directory
  - If no path is provided, the file is exported to the current working directory
  - File is named render.gif
- `env.reset_render()` Resets the progress of the renderer
  - Clears all images and restarts the gif
  - Without calling this, the gif will span multiple trials

# Future Improvements

- Add all dependencies to setup.py
- Working X-Plane Renderer example with docs
- Working ML examples with docs
- Scale render graph axis to include plane when it leaves bounds
- Include desired flight path in render graphs
- Allow custom implementation of cutoff constants

# References

 - [PyFME](https://github.com/AeroPython/PyFME)
 - [SymPy](https://docs.sympy.org)
 - [Vector Rotations](http://paulbourke.net/geometry/rotate/)
 - [Parametric Curves 3D](https://christopherchudzicki.github.io/MathBox-Demos/parametric_curves_3D.html)
 - [CalcPlot3D](https://www.monroecc.edu/faculty/paulseeburger/calcnsf/CalcPlot3D/)
